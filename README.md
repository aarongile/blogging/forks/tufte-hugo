# tufte-hugo

Personal Tufte CSS adapted for use with Hugo static site generator. 
  
- https://github.com/edwardtufte/tufte-css.git
- https://github.com/clayh53/tufte-jekyll.git

# LICENSE

Released under the MIT license. See [LICENSE](https://gitlab.com/aarongile/blog/forks/tufte-hugo/-/blob/main/LICENSE).

